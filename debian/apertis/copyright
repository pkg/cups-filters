Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 1993-2007, Easy Software Products
License: BSD-4-clause and/or Expat and/or GPL-2 and/or GPL-3 and/or LGPL-2 and/or LGPL-2.1

Files: backend/* filter/* mime/*
Copyright: 1993-2007 Easy Software Products
 2007-2011 Apple Inc.
 2012 Canonical Ltd.
 2006-2012 BBR Inc.
 2008-2016 Till Kamppeter
 2008,2012 Tobias Hoffmann
 2003-2006 Red Hat, Inc.
 2003-2006 Tim Waugh
 2012 Franz Pförtsch
 2012 Tomáš Chvátal
 2010 Neil 'Superna' Armstrong
 2015-2016 Samuel Thibault
 2008 Lars Karlitski (formerly Übernickel)
License: GPL-2+

Files: cupsfilters/*
Copyright: 2007-2016 Apple Inc
 1993-2007 by Easy Software Products
 2011-2013, Richard Hughes
 2011, Tim Waugh
 2013-2019, Till Kamppeter
 2014, Joseph Simon
 2017, Sahil Arora
 2018-2019, Deepak Patankar
License: LGPL-2

Files: scripting/* scripting/php/*
Copyright: 2007-2011, Apple Inc
 1993-2007, Easy Software Products
License: GPL-2

Files: filter/pdftopdf/*
Copyright: 2012 Tobias Hoffmann
 2006-2011, BBR Inc
License: Expat

Files: backend/cups-brf.c
Copyright: 2015-2018, 2020, Samuel Thibault <samuel.thibault@ens-lyon.org>
License: Expat

Files: cupsfilters/colord.c
Copyright: 2011-2013, Richard Hughes
 2011, Tim Waugh
License: Expat

Files: cupsfilters/colord.h
Copyright: 2011-2013, Richard Hughes
License: Expat

Files: cupsfilters/colormanager.c
Copyright: 2014, Joseph Simon
 2011-2013, Richard Hughes
License: Expat

Files: cupsfilters/colormanager.h
Copyright: 2014, Joseph Simon
License: Expat

Files: cupsfilters/ipp.c
 cupsfilters/ipp.h
Copyright: no-info-found
License: LGPL-2.1+

Files: cupsfilters/kmdevices.h
Copyright: 2014, Joseph Simon <jsimon383@gmail.com>
License: BSD-2-clause

Files: data/*
Copyright: no-info-found
License: GPL-2

Files: debian/*
Copyright: 1999-2003, Jeff Licquia <licquia@debian.org>
   2003-2007, Kenshi Muto <kmuto@debian.org>
   2007-2012, Martin Pitt <mpitt@debian.org>
   2012-2015, Till Kamppeter <till.kamppeter@gmail.com>
   2013-2019, Didier Raboud <odyx@debian.org>
License: GPL-2+

Files: debian/local/apport-hook.py
Copyright: 2009, Canonical Ltd.
License: GPL-2+

Files: debian/not-installed
Copyright: usr/share/doc/cups-filters/COPYING
License: GPL-2+

Files: drv/*
Copyright: 2015-2018, 2020, Samuel Thibault <samuel.thibault@ens-lyon.org>
License: Expat

Files: filter/banner.c
 filter/banner.h
Copyright: 2012, Canonical Ltd.
License: GPL-3

Files: filter/bannertopdf.c
 filter/pdf.cxx
Copyright: 2018, Sahil Arora <sahilarora.535@gmail.com>
 2013, ALT Linux, Andrew V. Stepanov <stanv@altlinux.com>
 2012, Canonical Ltd.
License: GPL-3

Files: filter/braille/*
Copyright: 2015-2018, 2020, Samuel Thibault <samuel.thibault@ens-lyon.org>
License: Expat

Files: filter/foomatic-rip/*
Copyright: 2008, Till Kamppeter <till.kamppeter@gmail.com>
 2008, Lars Karlitski (formerly Uebernickel) <lars@karlitski.net>
License: GPL-2+

Files: filter/getline.c
Copyright: 1993, Free Software Foundation, Inc.
License: GPL-2+

Files: filter/gstopxl
 filter/imagetops
 filter/texttops
Copyright: 2012, Till Kamppeter <till.kamppeter@gmail.com>
License: GPL-2+

Files: filter/gstoraster.c
Copyright: 2011-2013, Richard Hughes
 2011, Tim Waugh
 2008-2016, Till Kamppeter
License: Expat

Files: filter/mupdftoraster.c
Copyright: 2016, Pranjal Bhor
 2011-2013, Richard Hughes
 2011, Tim Waugh
 2008-2016, Till Kamppeter
License: Expat

Files: filter/pdf.h
Copyright: 2018, Sahil Arora <sahilarora.535@gmail.com>
 2012, Canonical Ltd.
License: GPL-3

Files: filter/pdftoraster.cxx
Copyright: 2019, Tanmay Anand.
 2012-2019, Till Kamppeter
 2008-2011, BBR Inc.
License: Expat

Files: filter/rastertopclm
Copyright: 2017, Sahil Arora <sahilarora.535@gmail.com>
License: GPL-2+

Files: filter/rastertopdf.cpp
Copyright: 2014, @author Sahil Arora <sahilarora.535@gmail.com> 2017
 2010, @author Tobias Hoffmann <smilingthax@gmail.com> 2012
License: GPL-3+

Files: filter/rastertops.c
Copyright: 2016, @author Neil Superna Armstrong <superna9999@gmail.com> (C) 2010
 2012, @author Till Kamppeter <till.kamppeter@gmail.com> 2014
License: GPL-3+

Files: filter/strcasestr.c
Copyright: 1990, 1993, The Regents of the University of California.
License: BSD-4-Clause-UC

Files: filter/unirast.h
Copyright: no-info-found
License: GPL-3+

Files: filter/urftopdf.cpp
Copyright: 2010, @author Tobias Hoffmann <smilingthax@gmail.com> 2012
License: GPL-3+

Files: fontembed/*
Copyright: 2008, 2012, Tobias Hoffmann.
License: Expat

Files: install-sh
Copyright: 2008, 2009, Apple Inc.
License: HPND-sell-variant

Files: ln-srf
Copyright: no-info-found
License: public-domain

Files: m4/*
Copyright: 2000-2008, Free Software Foundation, Inc.
License: GPL-2+ with Autoconf-2.0.AND_Autoconf-2.0~g10 exception

Files: m4/ax_compare_version.m4
Copyright: 2008, Tim Toolan <toolan@ele.uri.edu>
License: FSFAP

Files: m4/ax_cxx_compile_stdcxx.m4
Copyright: 2016, Krzesimir Nowak <qdlacz@gmail.com>
 2015, Paul Norman <penorman@mac.com>
 2015, Moritz Klammler <moritz@klammler.eu>
 2014, 2015, Google Inc.; contributedAlexey Sokolov <sokolov@google.com>
 2013, Roy Stogner <roystgnr@ices.utexas.edu>
 2012, Zack Weinberg <zackw@panix.com>
 2008, Benjamin Kosnik <bkoz@redhat.com>
License: FSFAP

Files: mime/braille.convs
 mime/braille.types
Copyright: 2015-2018, 2020, Samuel Thibault <samuel.thibault@ens-lyon.org>
License: Expat

Files: ppd/*
Copyright: no-info-found
License: GPL

Files: utils/*
Copyright: no-info-found
License: LGPL-2.1+

Files: utils/driverless-fax.in
Copyright: 2020, Nidhi Jain
 2012-2020, Till Kamppeter <till.kamppeter@gmail.com>
License: GPL-2+

Files: m4/basic-directories.m4 mime/cupsfilters.convs.in mime/cupsfilters.types
Copyright: 1993-2007 Easy Software Products
 2007-2011 Apple Inc.
 2012 Canonical Ltd.
 2006-2012 BBR Inc.
 2008-2016 Till Kamppeter
 2008,2012 Tobias Hoffmann
 2003-2006 Red Hat, Inc.
 2003-2006 Tim Waugh
 2012 Franz Pförtsch
 2012 Tomáš Chvátal
 2010 Neil 'Superna' Armstrong
 2015-2016 Samuel Thibault
 2008 Lars Karlitski (formerly Übernickel)
License: GPL-2+

Files: backend/beh.c
Copyright: 2015, Till Kamppeter
 2008-2015, Apple Inc
License: GPL-2

Files: backend/implicitclass.c
Copyright: 2015, Till Kamppeter
 2008-2015, Apple Inc
 2018-2019, Deepak Patankar
License: GPL-2

Files: cupsfilters/image.pgm cupsfilters/image.ppm cupsfilters/pdftoippprinter.c cupsfilters/ppdgenerator.c cupsfilters/ppdgenerator.h cupsfilters/raster.c cupsfilters/raster.h cupsfilters/testcmyk.c cupsfilters/testrgb.c
Copyright: 2007-2016 Apple Inc
 1993-2007 by Easy Software Products
 2011-2013, Richard Hughes
 2011, Tim Waugh
 2013-2019, Till Kamppeter
 2014, Joseph Simon
 2017, Sahil Arora
 2018-2019, Deepak Patankar
License: LGPL-2

Files: data/form_english_in.odt data/form_russian_in.odt
Copyright: 2012, Canonical Ltd
License: GPL-3

Files: drv/cupsfilters.drv
Copyright: 2012-2016 Till Kamppeter
License: Expat

Files: filter/imagetopdf.c
Copyright: 1993-2007, Easy Software Products
License: GPL-2

Files: filter/pcl-common.c filter/pcl-common.h filter/rastertopclx.c scripting/php/phpcups.php
Copyright: 2007-2011, Apple Inc
 1993-2007, Easy Software Products
License: GPL-2

Files: filter/pdftops.c filter/sys5ippprinter.c filter/texttotext.c
Copyright: 2011-2013 Till Kamppeter
 2007-2011 Apple Inc
 1997-2006 Easy Software Products
License: GPL-2

Files: filter/pdftops.c filter/sys5ippprinter.c filter/texttotext.c
Copyright: 2011-2013, Till Kamppeter
 2007-2011, Apple Inc
 1997-2006, Easy Software Products
License: GPL-2

Files: filter/pdfutils.c filter/pdfutils.h
Copyright: 2008, Tobias Hoffmann
License: GPL-2

Files: filter/texttopdf.c
Copyright: 2008, 2012, Tobias Hoffmann
 2007, Apple Inc
 1993-2007, Easy Software Products
License: GPL-2

Files: mime/cupsfilters.convs.in mime/cupsfilters.types
Copyright: 2007-2011, Apple Inc
 1997-2007, Easy Software Products
 2012-2016, Till Kamppeter
 2017, Sahil Arora
License: GPL-2

Files: ppd/pxlcolor.ppd ppd/pxlmono.ppd
Copyright: 2001-2005 Easy Software Products
License: GPL-2+
